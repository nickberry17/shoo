# README

Shoo lets you share a link from your Mac to another Mac on the network.  All you do is paste the link and click OK.

Shoo requires just a little bit of setup.

### How do I get set up? ###

Edit the file to use the correct IP address of the Mac you want to shoo to.

Open Shoo.AppleScript and edit it to your specifications, then export it as an Application (.app).  Optionally set an icon by copying applet.icns to Shoo > Contents > Resources

On your target Mac (the one you'll be shooing to) go Sharing in System Preferences, and enable Remote Apple Events.  Make sure the user account you want to authenticate with is in the list, or just use an account that exists on the remote Mac.  It may be helpful to also setup your target Mac to use "DHCP with a Stacic IP" network settings.  I highly recommend this for optimal functionality.

### What do I need to change in the source? ###

You'll need to write the EPPC:// string (which is a URI) to use your target IP address, username and password.  Like this:

```
eppc://username:password@192.168.10.101
```
Example
```
eppc://nickberry:B1tBuck3t@192.168.13.37
```

### Rationale ###

I often like to switch between watching videos on my Macbook to the Mac running as a HTPC.  Rather than retyping the link on the MacMini (the HTPC) I wanted to quickly share the link, and let it automatically start playing if possible.  Looking into a Swift/Cocoa implementation seemed overly complicated as you have to manage network sockets, and more.  I had a hunch that this could be done using Apple Events via AppleScript, and that hunch was correct.  

Technical Stuff

Using the EPPC protocol which handles remote system events in a Bonjour environment, a command with a message can be sent to another Mac that has been set up to execute Remote Apple Events.  As it turns out, this Event can have a payload, and OS X delegates parsing the payload to whatever application is being messaged.  In the case of Shoo, we are sending an event to Google Chrome, if it is installed (you can change it to any browser you want), to "open a location", on the MacMini.  The location is the url_text which is parsed as a URL, regardless of whether the string is a valid URL or not.  From that point on, any errors in the URL will be handled by the browser, meaning malformed URL's will not break Shoo.

Known Problems

If the target browser app on the target machine is not open, Shoo may throw an error saying "the software is not running".  

This can be solved by opening the browser on your target machine.  You could also set the browser as a login item so it's always open, even after a reboot.